from app.database.crud import get_internship_models, save_internship, save_contact_form, get_contact_form_models, \
    remove_contact_request, get_internship_model
from app.schemas import schemas
from app.database import models
from typing import List

from app.schemas.schemas import InternshipResponse


def get_internship_schemas() -> List[InternshipResponse]:
    internship_models = get_internship_models()
    internships = []

    for internship_model in internship_models:
        internship_id = internship_model.id
        name = internship_model.name
        description = internship_model.description
        internship = schemas.InternshipResponse(id=internship_id,
                                                name=name,
                                                description=description)
        internships.append(internship)
    return internships


def get_internship_schema(internship_id) -> InternshipResponse:
    internship_model = get_internship_model(internship_id)
    internship_id = internship_model.id
    name = internship_model.name
    description = internship_model.description
    return schemas.InternshipResponse(id=internship_id,
                                      name=name,
                                      description=description)


def create_internship(internship: schemas.InternshipRequest):
    internship_model = models.Internship(name=internship.name,
                                         description=internship.description)
    saved_internship_model = save_internship(internship_model)
    return InternshipResponse(id=saved_internship_model.id,
                              name=saved_internship_model.name,
                              description=saved_internship_model.description)


def create_contact_form(contact_form: schemas.ContactFormRequest):
    contact_form_model = models.ContactForm(name=contact_form.name,
                                            telegram_alias=contact_form.telegram,
                                            type=contact_form.type,
                                            post_id=contact_form.id)
    save_contact_form(contact_form_model)


def get_contact_form_schemas() -> List[schemas.ContactFormResponse]:
    contact_form_models: List[models.ContactForm] = get_contact_form_models()
    contact_forms = []

    for contact_form in contact_form_models:
        id = contact_form.id
        name = contact_form.name
        post_id = contact_form.post_id
        type = contact_form.type
        telegram_alias = contact_form.telegram_alias

        contact_form = schemas.ContactFormResponse(id=id,
                                                   telegram=telegram_alias,
                                                   name=name,
                                                   post_id=post_id,
                                                   type=type)
        contact_forms.append(contact_form)

    return contact_forms


def resolve_contact_request(contact_form_id):
    remove_contact_request(contact_form_id)
