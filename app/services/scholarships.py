from app.database.crud import get_scholarship_models, save_scholarship, save_scholarship_picture, \
    get_scholarship_picture
from app.database.models import ScholarshipPicture
from app.schemas import schemas
from app.database import models
from typing import List

from app.schemas.schemas import ScholarshipResponse


def get_scholarship_schemas() -> List[ScholarshipResponse]:
    scholarship_models = get_scholarship_models()
    scholarships = []

    for scholarship_model in scholarship_models:
        scholarship_id = scholarship_model.id
        name = scholarship_model.name
        description = scholarship_model.description
        scholarship = schemas.ScholarshipResponse(id=scholarship_id,
                                                  name=name,
                                                  description=description)
        scholarships.append(scholarship)
    return scholarships


def create_scholarship(scholarship: schemas.ScholarshipRequest):
    scholarship_model = models.Scholarship(name=scholarship.name,
                                           description=scholarship.description)
    saved_scholarship_model = save_scholarship(scholarship_model)
    return ScholarshipResponse(id=saved_scholarship_model.id,
                               name=saved_scholarship_model.name,
                               description=saved_scholarship_model.description)


def create_scholarship_picture(scholarship_id, picture_binary):
    scholarship_picture = ScholarshipPicture(scholarship_id=scholarship_id,
                                             picture=picture_binary)
    save_scholarship_picture(scholarship_picture)


def fetch_scholarship_picture(scholarship_id):
    return get_scholarship_picture(scholarship_id)