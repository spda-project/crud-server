from fastapi import APIRouter
from typing import List
from app.schemas.schemas import *
from app.services import internships as internship_service
from app.services.internships import get_internship_schema

router = APIRouter(prefix="/internship", tags=["internship"])


@router.get("/", response_model=List[InternshipResponse])
async def get_all_internships() -> List[InternshipResponse]:
    return internship_service.get_internship_schemas()


@router.get("/{internship_id}", response_model=InternshipResponse)
async def get_internship(internship_id: int) -> InternshipResponse:
    return get_internship_schema(internship_id)


@router.post("/", response_model=InternshipResponse)
async def create_internship(internship: InternshipRequest) -> InternshipResponse:
    return internship_service.create_internship(internship)
