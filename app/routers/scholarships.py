from fastapi import APIRouter
from typing import List
from app.schemas.schemas import *
from app.services import scholarships as scholarship_service

router = APIRouter(prefix="/scholarship", tags=["scholarship"])


@router.get("/", response_model=List[ScholarshipResponse])
async def get_all_scholarships() -> List[ScholarshipResponse]:
    return scholarship_service.get_scholarship_schemas()


@router.post("/", response_model=ScholarshipResponse)
async def create_scholarship(scholarship: ScholarshipRequest) -> ScholarshipResponse:
    return scholarship_service.create_scholarship(scholarship)

