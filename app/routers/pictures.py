import io

from fastapi import APIRouter, UploadFile, File, Form
from fastapi.responses import Response

from requests import Response
from starlette.responses import StreamingResponse

from app.services.scholarships import create_scholarship_picture, fetch_scholarship_picture

router = APIRouter(prefix="/picture", tags=["pictures"])
upload_dir = "data/pictures"


@router.post("/upload")
async def create_upload_file(scholarship_id=Form(...), picture_file: UploadFile = File(...)):
    create_scholarship_picture(scholarship_id, picture_file.file.read())


@router.get("/download/{scholarship_id}")
async def download_image(scholarship_id: int):
    picture_binary = fetch_scholarship_picture(scholarship_id)
    image_stream = io.BytesIO(picture_binary)
    return StreamingResponse(content=image_stream, media_type="image/png")
