from fastapi import APIRouter
from typing import List
from app.schemas.schemas import *
from app.services import internships as internship_service

router = APIRouter(prefix="/contact-form", tags=["internship"])


@router.post("/")
async def create_contact_form(contact_form: ContactFormRequest):
    internship_service.create_contact_form(contact_form)


@router.get("/", response_model=List[ContactFormResponse])
async def get_contact_forms():
    return internship_service.get_contact_form_schemas()


@router.delete("/{contact_form_id}")
async def resolve_contact_request(contact_form_id: int):
    internship_service.resolve_contact_request(contact_form_id)
