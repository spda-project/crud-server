from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, LargeBinary

Base = declarative_base()


class Internship(Base):
    __tablename__ = 'internship'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    description = Column(String(1024))


class Scholarship(Base):
    __tablename__ = 'scholarship'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    description = Column(String(1024))


class ScholarshipPicture(Base):
    __tablename__ = 'scholarship_picture'

    scholarship_id = Column(Integer, ForeignKey('scholarship.id', ondelete='CASCADE'), primary_key=True)
    picture = Column(LargeBinary)


class ContactForm(Base):
    __tablename__ = 'contact_form'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    telegram_alias = Column(String(255))
    type = Column(String(255))
    post_id = Column(Integer)
