from app.database.session_provider import get_session
from app.database.models import Internship, Scholarship, ScholarshipPicture, ContactForm
from typing import List


def get_internship_models() -> List[Internship]:
    session = get_session()
    return session.query(Internship).all()


def get_internship_model(internship_id):
    session = get_session()
    return session.query(Internship)\
        .filter_by(id=internship_id)\
        .first()


def save_internship(internship: Internship) -> Internship:
    session = get_session()
    session.add(internship)
    session.commit()
    return internship


def get_scholarship_models() -> List[Scholarship]:
    session = get_session()
    return session.query(Scholarship).all()


def save_scholarship(scholarship: Scholarship) -> Scholarship:
    session = get_session()
    session.add(scholarship)
    session.commit()
    return scholarship


def save_scholarship_picture(scholarship_picture: ScholarshipPicture):
    session = get_session()
    session.add(scholarship_picture)
    session.commit()


def get_scholarship_picture(scholarship_id):
    session = get_session()
    return session.query(ScholarshipPicture)\
        .filter_by(scholarship_id=scholarship_id)\
        .first().picture


def save_contact_form(contact_form):
    session = get_session()
    session.add(contact_form)
    session.commit()


def get_contact_form_models():
    session = get_session()
    return session.query(ContactForm).all()


def remove_contact_request(contact_request_id):
    session = get_session()
    session.query(ContactForm).filter_by(id=contact_request_id).delete()
    session.commit()
