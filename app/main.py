from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.database.models import Base
from app.database.session_provider import engine
from app.routers.internships import router as internship_router
from app.routers.pictures import router as picture_router
from app.routers.contact_forms import router as contact_form_router
from app.routers.scholarships import router as scholarship_router
from prometheus_fastapi_instrumentator import Instrumentator

Base.metadata.create_all(engine)

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(internship_router, prefix="/crud-server/api/v1")
app.include_router(scholarship_router, prefix="/crud-server/api/v1")
app.include_router(picture_router, prefix="/crud-server/api/v1")
app.include_router(contact_form_router, prefix="/crud-server/api/v1")

Instrumentator().instrument(app).expose(app, endpoint="/crud-server/metrics")
