from pydantic import BaseModel
from typing import Optional


class InternshipResponse(BaseModel):
    id: int
    name: str
    description: str


class InternshipRequest(BaseModel):
    name: str
    description: str


class ScholarshipResponse(BaseModel):
    id: int
    name: str
    description: str


class ScholarshipRequest(BaseModel):
    name: str
    description: str


class ContactFormRequest(BaseModel):
    telegram: str
    name: str
    id: int
    type: str


class ContactFormResponse(BaseModel):
    id: int
    telegram: str
    name: str
    post_id: int
    type: str
